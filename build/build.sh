#!/bin/bash

# Requirements:
#
# * 'optipng'
# * 'jpegoptim'
# * 'npm' (Node.js)
# * node package 'minify'

set -e

# Change directory to the parent folder of this script

source_dir="$( dirname "${BASH_SOURCE[0]}" )"
cd "$source_dir"

# Search dependencies

NPM=$(command -v npm) || true
OPTIPNG=$(command -v optipng) || true
JPEGOPTIM=$(command -v jpegoptim) || true
MINIFY=$(command -v minify) || true

if [ ! $MINIFY ]; then
  MINIFY=$(command -v minify.js) || true
fi

if [ -f ../node_modules/minify/bin/minify.js ]; then
  MINIFY="../node_modules/minify/bin/minify.js"
fi

# If there are missing dependencies, print install instructions

if [[ ! $OPTIPNG || ! $JPEGOPTIM || ! $NPM || ! $MINIFY ]]; then

  echo -e "\nError: The following dependencies are missing:\n"

  if [ ! $OPTIPNG ]; then
    echo "\t* 'optipng'"
    install_list="optipng"
  fi
  if [ ! $JPEGOPTIM ]; then
    echo "\t* 'jpegoptim'"
    install_list="$install_list jpegoptim"
  fi
  if [ ! $NPM ]; then
    echo "\t* 'npm'"
    install_list="$install_list npm"
  fi
  if [ ! $MINIFY ]; then
    echo "\t* node package 'minify'"
  fi

  echo -e "\nIf you are on Fedora, you can install missing dependencies by running:\n"

  if [ $install_list ]; then
    echo "\$ sudo dnf install $install_list"
  fi
  if [ ! $MINIFY ]; then
    echo "\$ npm install minify"
    echo -e "\nDon't forget to add ./node_modules/minify/bin/ to your PATH in ~/.bashrc\n"
  fi

  exit 1

fi


# Minify a file in the format name.extension to name.min.extension
# Usage: minify_file <path_to_file_without_extension> <extension>
function minify_file {
  if [ -f $1.$2 ]; then
    echo -n "Minifying '$1.$2'  ...  "
    cat $1.$2 | $MINIFY --$2 > $1.min.$2 && echo "OK."
  else
    echo "Error: $1.$2 not found"
  fi
}

function echo_blue {
  echo -e "\033[1;34m\n$1\n\033[0m"
}

# Main program

echo_blue "(1/3): Optimize PNG images"
optipng ../static/img/*.png

echo_blue "(2/3): Optimize JPG images"
jpegoptim ../static/img/*.jpg

echo_blue "(3/3): Minify JS, CSS and HTML files"
minify_file ../static/js/etherpad js
minify_file ../static/vendor/fontello/css/fontello css
# Special case for html
echo -n "Minifying ../index.full.html  ...  "
cat ../index.full.html | $MINIFY --html > ../index.html && echo "OK."

echo_blue "Build successfull"
